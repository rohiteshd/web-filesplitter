counterStr=""

# check if counter specified
if [ $# -gt 0 ]; then
	counterStr="$1."
fi

# form command str (required for exhaustive flag)
commandStr=$HOME"/Developer/testScripts/CopyCodeRepo.sh web-FileSplitter"
if [[ $# -gt 1 && $2 == "exhaustive" ]]; then
	commandStr=$commandStr" exhaustive"
fi

# run copy repo command
echo $counterStr `$commandStr`
