package FileSplitter.Main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import FileSplitter.Utils.JSON.JSONArray;
import FileSplitter.Utils.JSON.JSONException;
import FileSplitter.Utils.JSON.JSONObject;

public class FileSplitter {
	// constants
	static final int SINGLECHUNK_SIZE = 1024*1000; // 1 MB
	// static final int CHUNKNUMBERS = 10000; // 10 MB
	static final int FILESIZE = 10; // 10 MB
	static final String FILELIST_FILENAME = "FileDetails_rdFileSplitter.txt";
	
	String fileLocation_src, fileLocation_dst;
	String fileName, fileExtension;
	int fileSize;
	JSONObject fileDetails_json;
	boolean isFileOrFolder;
	
	// constructor
	public FileSplitter(String filePath_src, String filePath_dst, int fileSize, boolean isFileOrFolder){
		// get directory path and file name
		fileName = filePath_src.substring(filePath_src.lastIndexOf("/")+1, filePath_src.lastIndexOf("."));
		fileExtension = filePath_src.substring(filePath_src.lastIndexOf(".")+1);
		fileLocation_src = filePath_src.substring(0, filePath_src.lastIndexOf("/")+1);
		fileLocation_dst = StringUtils.isValid(filePath_dst) ? filePath_dst : fileLocation_src+fileName;
		this.isFileOrFolder = isFileOrFolder;
		
		// make directory to hold all file parts (if not folder split)
		if(isFileOrFolder && !(new File(fileLocation_src + fileName)).isDirectory())
			new File(fileLocation_src + fileName).mkdir();
		
		// instantiate json and fill file details
		fileDetails_json = new JSONObject();
		try {
			fileDetails_json.put("fileName", fileName+"."+fileExtension);
		} catch(JSONException e){
			System.out.println("JSONException");
			e.printStackTrace();
		}
		
		// set filesize
		this.fileSize = fileSize>0 ? fileSize : FILESIZE;
	}
	
	// set up file split/stitch
	public void start(){
		splitFile();
	}
	
	// clean up after file split/stitch
	public void cleanUp(){
		// write json to file if file
		if(isFileOrFolder)
			try {
				// add file or folder status
				fileDetails_json.put("isFileOrFolder", true);
				
				// write file details
				FileUtils.writeStringToFile(new File(fileLocation_src + fileName + "/" + FileSplitter.FILELIST_FILENAME), fileDetails_json.toString());
			} catch(JSONException e){
				System.out.println("JSONException");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("IOException");
				e.printStackTrace();
			}
	}
	
	// copy src file chunk into tmp file
	private void splitFile(){
		FileInputStream fis = null;
		FileOutputStream fos = null;
		byte[] b = new byte[SINGLECHUNK_SIZE];
		int count = 0, fileCount = 0;
		boolean isLastWriteFileClosed = false;
		JSONArray fileList_jsonArr = new JSONArray();
		
		try {
			// read source file
			fis = new FileInputStream(new File(fileLocation_src + fileName + "." + fileExtension));
			while(fis.read(b) != -1){
				// create new file
				if(count == 0){
					fos = new FileOutputStream(new File(fileLocation_dst + "/" + fileName + "." + fileCount));
					isLastWriteFileClosed = false;
				}
				
				// write to file
				fos.write(b);
				count++;
			
				// check if new write file needed
				if(count == fileSize){
					fos.close();
					count = 0; fileCount++; isLastWriteFileClosed = true;
					
					// update file list in the json array
					fileList_jsonArr.put(fileName+"."+(fileCount-1));
					
					// marker
					System.out.println("Done Copying File : " + fileName+"."+(fileCount-1));
				}
			}
			
			// close file if not done
			if(!isLastWriteFileClosed){
				fos.close(); fileCount++;
					
				// update file list in the json array
				fileList_jsonArr.put(fileName+"."+(fileCount-1));
				
				// marker
				System.out.println("Done Copying File : " + fileName+"."+(fileCount-1));
			}
		} catch(FileNotFoundException e){
			System.out.println("FileNotFoundException");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException");
			e.printStackTrace();
		} finally {
			try {
				if(fis != null)
					fis.close();
				
				// write the json array
				fileDetails_json.put("fileList", fileList_jsonArr);
			} catch (IOException e) {
				System.out.println("IOException");
				e.printStackTrace();
			} catch (JSONException e) {
				System.out.println("JSONException");
				e.printStackTrace();
			}
		}
	}

	// getter
	public JSONObject getFileDetails_json() {
		return fileDetails_json;
	}
}