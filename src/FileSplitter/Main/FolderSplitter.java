package FileSplitter.Main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import FileSplitter.Utils.JSON.JSONArray;
import FileSplitter.Utils.JSON.JSONException;
import FileSplitter.Utils.JSON.JSONObject;

public class FolderSplitter {
	// constants
	static final String SPLITFOLDER_APPEND = "_split";
	
	// folder related
	String folderLocation_dst, folderLocation_src;
	ArrayList<String> fileLocations_src, fileLocations_dst;
	int fileSize;
	JSONObject fileDetails_json;
	
	// constructor
	public FolderSplitter(String folderPath, int fileSize){
		String folderName;
		File folder_src = new File(folderPath);
		
		// get folder name and set src and dst folder locations
		folderLocation_src = folderPath;
		folderName = folderPath.substring(folderPath.lastIndexOf("/", folderPath.lastIndexOf("/")-1)+1, folderPath.length()-1) + SPLITFOLDER_APPEND;
		folderLocation_dst = folderPath.substring(0, folderPath.lastIndexOf("/", folderPath.lastIndexOf("/")-1)) + "/" + folderName;
		
		// make directory to hold all split files
		if(!(new File(folderLocation_dst)).isDirectory())
			new File(folderLocation_dst).mkdir();
		
		// make src and dst file list
		for(int x=0; x<folder_src.list().length; x++){
			// src file list
			if(fileLocations_src == null)
				fileLocations_src = new ArrayList<String>();
			fileLocations_src.add(folderLocation_src + folder_src.list()[x]);

			// dst file list
			if(fileLocations_dst == null)
				fileLocations_dst = new ArrayList<String>();
			fileLocations_dst.add(folderLocation_dst);
		}
		
		// instantiate json
		fileDetails_json = new JSONObject();
		try {
			fileDetails_json.put("folderName", folderName.substring(0, folderName.indexOf(SPLITFOLDER_APPEND)));
		} catch(JSONException e){
			System.out.println("JSONException");
			e.printStackTrace();
		}
		
		// set filesize
		this.fileSize = fileSize;
	}
	
	// set up file split/stitch
	public void start(){
		splitFolder();
	}
	
	// clean up after file split/stitch
	public void cleanUp(){
		try {
			// add file or folder status
			fileDetails_json.put("isFileOrFolder", false);
			
			// write file details
			FileUtils.writeStringToFile(new File(folderLocation_dst + "/" + FileSplitter.FILELIST_FILENAME), fileDetails_json.toString());
		} catch(JSONException e){
			System.out.println("JSONException");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IOException");
			e.printStackTrace();
		}
	}
	
	// split folder
	private void splitFolder(){
		FileSplitter fileSplitter;
		
		// json related
		JSONArray fileList_jsonArr = new JSONArray();
		
		try {
			// parse through the files in the folder and split them
			for(int x=0; fileLocations_src!=null && x<fileLocations_src.size(); x++){
				// call file splitter on each file
				fileSplitter = new FileSplitter(fileLocations_src.get(x), fileLocations_dst.get(x), fileSize, false);
				fileSplitter.start();
				
				// form the json
				fileList_jsonArr.put(fileSplitter.getFileDetails_json());
			}
			
			// put the json details into the main json
			this.fileDetails_json.put("folderList", fileList_jsonArr);
		} catch(JSONException e){
			System.out.println("JSONException");
			e.printStackTrace();
		}
	}
}