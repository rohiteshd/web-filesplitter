package FileSplitter.Main;

import java.util.Scanner;

public class Main {
	// action constants
	public static final String ACTION_SPLIT = "split";
	public static final String ACTION_STITCH = "stitch";
	
	// main function
	public static void main(String[] args){
		String filePath = null, userInput;
		boolean isFileOrFolder;
		int fileSize = 0;
		Scanner sc;
		
		// splitter related
		FileSplitter fileSplitter;
		FolderSplitter folderSplitter;
		
		// get user inputs
		sc = new Scanner(System.in);
		System.out.println("Please enter file path : ");
		userInput = sc.nextLine();
		sc.close();
		
		// get file path and chunk numbers if available
		if(StringUtils.isValid(userInput) && userInput.contains(" ") && userInput.split(" ")[userInput.split(" ").length-1].contains("size")) {
			fileSize = Integer.parseInt(userInput.split(" ")[userInput.split(" ").length-1].split("=")[1]);
			filePath = userInput.substring(0, userInput.lastIndexOf(" size"));
		} else
			filePath = userInput;

		
		// set file or folder flag
		isFileOrFolder = filePath.charAt(filePath.length()-1) != '/';
		
		// call file or folder splitter/stitcher
		if(isFileOrFolder){
			fileSplitter = new FileSplitter(filePath, null, fileSize, isFileOrFolder);
			fileSplitter.start();
			fileSplitter.cleanUp();
		} else {
			folderSplitter = new FolderSplitter(filePath, fileSize);
			folderSplitter.start();
			folderSplitter.cleanUp();
		}
		
		// marker
		System.out.println("Done!");
	}
}